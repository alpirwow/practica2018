namespace Todo.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Creator",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        Caption = c.String(nullable: false),
                        TaskDescription = c.String(nullable: false),
                        Priority = c.Byte(nullable: false),
                        StatusId = c.Int(nullable: false),
                        CreatorId = c.Int(nullable: false),
                        TegId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Creator", t => t.CreatorId, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusId, cascadeDelete: true)
                .ForeignKey("dbo.Teg", t => t.TegId, cascadeDelete: true)
                .Index(t => t.StatusId)
                .Index(t => t.CreatorId)
                .Index(t => t.TegId);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teg",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Tegs = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Task", "TegId", "dbo.Teg");
            DropForeignKey("dbo.Task", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Task", "CreatorId", "dbo.Creator");
            DropIndex("dbo.Task", new[] { "TegId" });
            DropIndex("dbo.Task", new[] { "CreatorId" });
            DropIndex("dbo.Task", new[] { "StatusId" });
            DropTable("dbo.Teg");
            DropTable("dbo.Status");
            DropTable("dbo.Task");
            DropTable("dbo.Creator");
        }
    }
}
