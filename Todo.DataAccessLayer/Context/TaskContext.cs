﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.DataAccessLayer.Entity;

namespace Todo.DataAccessLayer.Context
{
    public class TaskContext : DbContext
    {
        public TaskContext() 
        {
            Database.Connection.ConnectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=st;" +
                "Integrated Security=True;MultipleActiveResultSets=True";
        }

        public DbSet<TaskEntity> TasksModels { get; set; }

        public DbSet<StatusEntity> StatusModels { get; set; }

        public DbSet<TegEntity> TegModels { get; set; }

        public DbSet<CreatorEntity> CreatorModels { get; set; }
    }
}

