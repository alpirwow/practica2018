﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Todo.DataAccessLayer.Entity
{
    [Table("Task")]
    public class TaskEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime DateCreate { get; set; }

        public DateTime? DateEnd { get; set; }

        [Required]
        public string Caption { get; set; }

        [Required]
        public string TaskDescription { get; set; }

        [Required]
        public byte Priority { get; set; }
              
        public int StatusId { get; set; }

        [ForeignKey("StatusId")]
        public virtual StatusEntity Status { get; set; }

        public int CreatorId { get; set; }

        [ForeignKey("CreatorId")]
        public virtual CreatorEntity Creator { get; set; }

        public int TegId { get; set; }

        [ForeignKey("TegId")]
        public virtual TegEntity Teg { get; set; }
    }
}
