﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Todo.DataAccessLayer.Entity
{
    [Table("Teg")]
    public class TegEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Tegs { get; set; }

        public virtual List<TaskEntity> Task { get; set; }
    }
}
