﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.DataAccessLayer.Context;
using Todo.DataAccessLayer.Entity;

namespace Todo.DataAccessLayer.InitializerDBTodo
{
    public class CreatorContextInitializerDBTodo : DropCreateDatabaseAlways<TaskContext>
    {
        protected override void Seed(TaskContext context)
        {
            IList<CreatorEntity> creatorEntities = new List<CreatorEntity>();

            creatorEntities.Add(new CreatorEntity() { Id = 1, Name = "Alex" });
            creatorEntities.Add(new CreatorEntity() { Id = 2, Name = "Evgen" });
            creatorEntities.Add(new CreatorEntity() { Id = 3, Name = "Vlad" });

            context.CreatorModels.AddRange(creatorEntities);

            base.Seed(context);
        }

    }
}
